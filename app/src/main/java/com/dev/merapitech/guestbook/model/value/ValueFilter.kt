package com.dev.merapitech.guestbook.model.value

class ValueFilter {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: DataFilter

    class DataFilter{
        lateinit var page: String
        lateinit var per_page: String
        lateinit var total_page: String
        lateinit var result: ArrayList<ResultFilter>
        class ResultFilter{
            lateinit var id: String
            lateinit var name: String
            lateinit var email: String
            lateinit var phone: String
            lateinit var alamat: String
            lateinit var komentar: String
        }
    }
}