package darmawan.rifqi.eventmerapitech.retrofit

object UtilsApi {
    val BASE_URL_API = "http://206.189.154.135:5001/api/"

    val apiService: BaseApiService
        get() = RetrofitClient.getClient(BASE_URL_API).create(BaseApiService::class.java)
}