package com.dev.merapitech.guestbook.model.request

class RequestFilter{
    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var data: DataFilter
    class DataFilter{
        lateinit var page: String
    }
}