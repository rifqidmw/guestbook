package com.dev.merapitech.guestbook.utils

import com.dev.merapitech.guestbook.model.request.RequestFilter
import com.dev.merapitech.guestbook.model.request.RequestRegister
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST


/**
 * Created by XGibar on 27/10/2016.
 */
interface Interface {
    @POST("http://206.189.154.135:5001/api/v1")
    fun requestFilter(@Body requestFilter: RequestFilter): Call<ResponseBody>

    @POST("http://206.189.154.135:5001/api/v1")
    fun requestRegister(@Body requestRegister: RequestRegister): Call<ResponseBody>
}


