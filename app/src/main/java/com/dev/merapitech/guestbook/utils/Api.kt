package com.dev.merapitech.guestbook.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.dev.merapitech.guestbook.model.value.ValueFilter
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.text.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

/**
 * Created by argaimac on 4/5/17.
 */

object Api {
    private var instance: Api? = null
    var BASE_URL = "http://206.189.154.135:5001//api/v1/"
    var dataList: ArrayList<ValueFilter.DataFilter.ResultFilter> = ArrayList()

    lateinit var mProgressDialog: ProgressDialog
    var showLog = true
    private var formatter: NumberFormat? = null
    private var df: DecimalFormat? = null
    private var humanDate: DateFormat? = null

    val DEFAULT_RETRIES = 0

//    fun getSignature(signature: String): String? {
//        var post: String? = null
//        try {
//            post = Helper.SHA1(signature)
//        } catch (e: NoSuchAlgorithmException) {
//            e.printStackTrace()
//        }
//
//        return post
//    }

    fun getInstance(): Api {
        if (instance == null) {
            instance = Api
        }
        formatter = DecimalFormat("#,###,###")
        df = DecimalFormat("####0.00")
        humanDate = SimpleDateFormat("dd-MM-yyyy")
        return instance as Api
    }

    //INITIALIZE RETROFIT//
    fun initRetrofit(showLog: Boolean): Interface {
        val retrofit: Retrofit
        if (showLog == true) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build()
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        } else {
            val client = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit.create(Interface::class.java)
    }

    //RETRY DIALOG//
    fun retryDialog(

        context: Context,
        call: Call<ResponseBody>,
        callback: Callback<ResponseBody>,
        tanda: Int,
        dialog: Boolean
    ) {
        AlertDialog.Builder(context)
            .setTitle("Gagal Terhubung")
            .setMessage("Pastikan sudah terhubung dengan jaringan yang stabil")
            .setCancelable(false)
            .setNegativeButton("Keluar", DialogInterface.OnClickListener { arg0, arg1 ->
                (context as Activity).setResult(tanda)
                context.finish()
            }
            )
            .setPositiveButton("Coba lagi", DialogInterface.OnClickListener { arg0, arg1 ->
                if (dialog == true) {
                    mProgressDialog = ProgressDialog(context)
                    mProgressDialog.isIndeterminate = true
                    mProgressDialog.setCancelable(false)
                    mProgressDialog.setMessage("Loading...")
                    mProgressDialog.show()
                    call.clone().enqueue(object : RetryableCallback<ResponseBody>(call) {

                        override fun onFinalResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            mProgressDialog.dismiss()
                            callback.onResponse(call, response)
                        }

                        override fun onFinalFailure(call: Call<ResponseBody>, t: Throwable) {
                            mProgressDialog.dismiss()
                            callback.onFailure(call, t)
                        }
                    })
                } else {
                    call.clone().enqueue(object : RetryableCallback<ResponseBody>(call) {

                        override fun onFinalResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            mProgressDialog.dismiss()
                            callback.onResponse(call, response)
                        }

                        override fun onFinalFailure(call: Call<ResponseBody>, t: Throwable) {
                            mProgressDialog.dismiss()
                            callback.onFailure(call, t)
                        }
                    })
                }
            }).create().show()
    }

    //RETROFIT RETRIES//
    fun <T> enqueueWithRetry(
        context: Context,
        call: Call<T>,
        retryCount: Int,
        callback: Callback<T>,
        dialog: Boolean,
        message: String
    ) {
        if (dialog == true) {
            mProgressDialog = ProgressDialog(context)
            mProgressDialog.isIndeterminate = true
            mProgressDialog.setCancelable(false)
            mProgressDialog.setMessage(message)
            mProgressDialog.show()
            call.enqueue(object : RetryableCallback<T>(call, retryCount) {

                override fun onFinalResponse(call: Call<T>, response: Response<T>) {
                    mProgressDialog.dismiss()
                    callback.onResponse(call, response)
                }

                override fun onFinalFailure(call: Call<T>, t: Throwable) {
                    mProgressDialog.dismiss()
                    callback.onFailure(call, t)
                }
            })
        } else {
            call.enqueue(object : RetryableCallback<T>(call, retryCount) {

                override fun onFinalResponse(call: Call<T>, response: Response<T>) {
                    callback.onResponse(call, response)
                }

                override fun onFinalFailure(call: Call<T>, t: Throwable) {
                    callback.onFailure(call, t)
                }
            })
        }
    }

    fun <T> enqueueWithRetry(context: Context, call: Call<T>, callback: Callback<T>, dialog: Boolean, message: String) {
        enqueueWithRetry(context, call, DEFAULT_RETRIES, callback, dialog, message)
    }

    fun isCallSuccess(response: Response<*>): Boolean {
        val code = response.code()
        return code >= 200 && code < 400
    }
}

internal abstract class RetryableCallback<T> : Callback<T> {

    private var totalRetries = 0
    private val call: Call<T>
    private var retryCount = 0

    constructor(call: Call<T>, totalRetries: Int) {
        this.call = call
        this.totalRetries = totalRetries
    }

    constructor(call: Call<T>) {
        this.call = call
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (!Api.isCallSuccess(response))
            if (retryCount++ < totalRetries) {
                retry()
            } else
                onFinalResponse(call, response)
        else
            onFinalResponse(call, response)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        //        Log.e(TAG, t.getMessage());
        if (retryCount++ < totalRetries) {
            retry()
        } else
            onFinalFailure(call, t)
    }

    open fun onFinalResponse(call: Call<T>, response: Response<T>) {

    }

    open fun onFinalFailure(call: Call<T>, t: Throwable) {}

    private fun retry() {
        call.clone().enqueue(this)
    }

//    companion object {
//        private val TAG = RetryableCallback<*>::class.java.simpleName
//    }

}