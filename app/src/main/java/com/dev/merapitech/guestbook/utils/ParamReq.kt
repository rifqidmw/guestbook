package com.dev.merapitech.guestbook.utils

import android.annotation.SuppressLint
import android.content.Context
import com.dev.merapitech.guestbook.MainActivity
import com.dev.merapitech.guestbook.R
import com.dev.merapitech.guestbook.model.request.RequestFilter
import com.dev.merapitech.guestbook.model.request.RequestRegister
import okhttp3.ResponseBody
import retrofit2.Call

/**
 * Created by palapabeta on 02/02/18.
 */

@SuppressLint("StaticFieldLeak")
object ParamReq {

    var context: Context? = null
    private lateinit var APIInterface: Interface

    fun merapitech(): MainActivity {
        return context as MainActivity
    }

    fun req0104(hCode: String, context: Context, email: String, name: String, phone: String, alamat: String): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val data = RequestRegister.RegisterData()
        data.name = name
        data.email = email
        data.phone = phone
        data.alamat = alamat
        val params = RequestRegister()
        params.HeaderCode = hCode
        params.signature = context.getString(R.string.signature)
        params.data = data
        return APIInterface.requestRegister(params)
    }
    fun req0101(context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val params = RequestFilter()
        params.HeaderCode = "0101"
        params.signature = context.getString(R.string.signature)
        return APIInterface.requestFilter(params)
    }


}
