package com.dev.merapitech.guestbook

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import com.dev.merapitech.guestbook.adapter.FilterAdapter
import com.dev.merapitech.guestbook.model.request.RequestFilter
import com.dev.merapitech.guestbook.model.value.ValueFilter
import com.dev.merapitech.guestbook.utils.Api
import com.dev.merapitech.guestbook.utils.Handle
import com.dev.merapitech.guestbook.utils.ParamReq
import com.dev.merapitech.guestbook.utils.RetryableCallback
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    lateinit var view: View
    lateinit var mApiService: BaseApiService
    lateinit var filterAdapter: FilterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mApiService = UtilsApi.apiService

        button_check_in.setOnClickListener {
            startActivity(Intent(this, AddGuestActivity::class.java))
        }
        button_semua_check_in.setOnClickListener {
            startActivity(Intent(this@MainActivity, LatestCheckInActivity::class.java))
        }

        filterAdapter = FilterAdapter(this, Api.dataList, 1)

        recycler_latest.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_latest.itemAnimator = DefaultItemAnimator()
        recycler_latest.adapter = filterAdapter

        getData()
    }

    fun getData(){
        val data = RequestFilter()
        data.HeaderCode = "0101"
        data.signature = getString(R.string.signature)
        mApiService.getData(data)
            .enqueue(object : Callback<ValueFilter> {
                override fun onResponse(call: Call<ValueFilter>, response: Response<ValueFilter>) {
                    if (response.isSuccessful){
                        if (response.body()!!.status == "200"){
                            Api.dataList = response.body()!!.data.result
                            filterAdapter = FilterAdapter(this@MainActivity,Api.dataList, 1)
                            filterAdapter.notifyDataSetChanged()
                            recycler_latest!!.adapter = filterAdapter
                            txt_total_guest.text = Api.dataList.size.toString()
                        } else {
                            val message = response.body()!!.message
                            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Log.d("pg", "asdasd")
                        Toast.makeText(this@MainActivity, "Data Tidak Ditemukan", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ValueFilter>, t: Throwable) {
                    AlertDialog.Builder(this@MainActivity)
                        .setTitle("Gagal Terhubung")
                        .setMessage("Pastikan sudah terhubung dengan jaringan yang stabil")
                        .setCancelable(false)
                        .setNegativeButton("Keluar", DialogInterface.OnClickListener { arg0, arg1 ->
                            (this@MainActivity as Activity).setResult(1)
                            this@MainActivity.finish()
                        }
                        )
                        .setPositiveButton("Coba lagi", DialogInterface.OnClickListener { arg0, arg1 ->
                            startActivity(Intent(this@MainActivity, MainActivity::class.java))
                            finish()
                        }).create().show()
                }
            })

    }
}
