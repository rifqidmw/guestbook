package darmawan.rifqi.eventmerapitech.retrofit

import com.dev.merapitech.guestbook.model.request.RequestFilter
import com.dev.merapitech.guestbook.model.request.RequestRegister
import com.dev.merapitech.guestbook.model.value.ValueFilter
import com.dev.merapitech.guestbook.model.value.ValueRegister
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface BaseApiService {
    @POST("v1")
    fun getData(@Body requestFilter: RequestFilter): Call<ValueFilter>
    @POST("v1")
    fun registerGuest(@Body requestRegister: RequestRegister): Call<ValueRegister>
}