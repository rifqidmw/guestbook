package com.dev.merapitech.guestbook.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dev.merapitech.guestbook.R
import com.dev.merapitech.guestbook.model.value.ValueFilter

class FilterAdapter(val context: Context,var list: ArrayList<ValueFilter.DataFilter.ResultFilter> = arrayListOf(), var tipe: Int) : RecyclerView.Adapter<FilterAdapter.FilterHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterAdapter.FilterHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_check_in, parent, false)
        return FilterHolder(view)
    }

    override fun getItemCount(): Int {
        if (tipe == 1) {
            return Math.min(list.size, 5)
        } else {
            return list.size
        }
    }

    override fun onBindViewHolder(holder: FilterAdapter.FilterHolder, position: Int) {
        val data = list[position]
        holder.textName.text = data.name
        holder.textEmail.text = data.email
    }

    inner class FilterHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textName: TextView
        internal var textEmail: TextView


        init {

            textName = itemView.findViewById(R.id.text_guest_name)
            textEmail = itemView.findViewById(R.id.text_guest_mail)

        }
    }

    fun refreshAdapter(resultMember: List<ValueFilter.DataFilter.ResultFilter>) {
        this.list.addAll(resultMember)
        notifyItemRangeChanged(0, this.list.size)
    }
}

