package com.dev.merapitech.guestbook.model.request

class RequestRegister{

    lateinit var HeaderCode: String
    lateinit var signature: String
    lateinit var data: RegisterData

    class RegisterData{
        lateinit var email: String
        lateinit var name: String
        lateinit var phone: String
        lateinit var alamat: String
    }
}