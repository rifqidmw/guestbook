package com.dev.merapitech.guestbook

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import com.dev.merapitech.guestbook.adapter.FilterAdapter
import com.dev.merapitech.guestbook.model.request.RequestFilter
import com.dev.merapitech.guestbook.model.value.ValueFilter
import com.dev.merapitech.guestbook.utils.Api
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_latest_check_in.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.properties.Delegates

class LatestCheckInActivity : AppCompatActivity() {

    lateinit var mApiService: BaseApiService
    lateinit var filterAdapter: FilterAdapter

    private var isLoading: Boolean = false
    private var page by Delegates.notNull<Int>()
    private var totalPage by Delegates.notNull<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latest_check_in)

        mApiService = UtilsApi.apiService

        filterAdapter = FilterAdapter(this, Api.dataList, 2)

        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this@LatestCheckInActivity, MainActivity::class.java))
        }

        recycler_latest.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_latest.itemAnimator = DefaultItemAnimator()
        recycler_latest.adapter = filterAdapter
        recycler_latest!!.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        page = 1
        totalPage = 0

        getData()
        initListener()
    }

    fun getData() {
        val req = RequestFilter()
        req.HeaderCode = "0101"
        req.signature = getString(R.string.signature)
        val data = RequestFilter.DataFilter()
        data.page = Integer.toString(page)
        req.data = data
        mApiService.getData(req)
            .enqueue(object : Callback<ValueFilter> {
                override fun onResponse(call: Call<ValueFilter>, response: Response<ValueFilter>) {
                    if (response.isSuccessful) {
                        if (response.body()!!.status == "200") {
                            Api.dataList = response.body()!!.data.result
                            if (page == 1){
                                filterAdapter = FilterAdapter(this@LatestCheckInActivity, Api.dataList, 2)
                                filterAdapter.notifyDataSetChanged()
                                recycler_latest!!.adapter = filterAdapter
                            } else {
                                filterAdapter.refreshAdapter(Api.dataList)
                            }
                            totalPage = Integer.parseInt(response.body()!!.data.total_page)
                            Log.d("totalpage", Integer.toString(totalPage))
                        } else {
                            val message = response.body()!!.message
                            Toast.makeText(this@LatestCheckInActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Log.d("pg", "asdasd")
                        Toast.makeText(this@LatestCheckInActivity, "Data Tidak Ditemukan", Toast.LENGTH_SHORT).show()
                    }
                    hideLoading()
                }

                override fun onFailure(call: Call<ValueFilter>, t: Throwable) {
                    AlertDialog.Builder(this@LatestCheckInActivity)
                        .setTitle("Gagal Terhubung")
                        .setMessage("Pastikan sudah terhubung dengan jaringan yang stabil")
                        .setCancelable(false)
                        .setNegativeButton("Keluar", DialogInterface.OnClickListener { arg0, arg1 ->
                            (this@LatestCheckInActivity as Activity).setResult(1)
                            this@LatestCheckInActivity.finish()
                        }
                        )
                        .setPositiveButton("Coba lagi", DialogInterface.OnClickListener { arg0, arg1 ->
                            startActivity(Intent(this@LatestCheckInActivity, LatestCheckInActivity::class.java))
                            finish()
                        }).create().show()
                }
            })
    }

    private fun initListener(){
        recycler_latest!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager?.itemCount
                val lastVisiblePosition = linearLayoutManager?.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                Log.d("count item: ", "countItem: $countItem")
                Log.d("last visible position: ", "lastVisiblePosition: $lastVisiblePosition")
                Log.d("last position: ", "isLastPosition: $isLastPosition")
                if (!isLoading && isLastPosition && page < totalPage) {
                    showLoading(true)
                    page = page.let { it.plus(1) }
                    getData()
                }
            }
        })
    }

    private fun showLoading(isRefresh: Boolean) {
        isLoading = true
        progress_bar_horizontal_activity_main.visibility = View.VISIBLE
        recycler_latest!!.visibility.let {
            if (isRefresh) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    private fun hideLoading() {
        isLoading = false
        progress_bar_horizontal_activity_main.visibility = View.GONE
        recycler_latest!!.visibility = View.VISIBLE
    }
}
