package com.dev.merapitech.guestbook.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import com.dev.merapitech.guestbook.R
import com.dev.merapitech.guestbook.model.value.ValueFilter
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by palapabeta on 03/02/18.
 */

class Handle {

    private val session: Session? = null

    companion object {

//        fun handleLogin(sjson: String, context: Context): Boolean {
//
//            val session = Session(context)
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//                    session.save(
//                        context.getString(R.string.apikey),
//                        jsonObject.getJSONObject("data").getString(context.getString(R.string.apikey)).toString()
//                    )
//                    return true
//
//                } else {
//
//                    return false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
//
//        fun handleProd(sjson: String, context: Context, code: String): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//                val succses = Integer.parseInt(jsonObject.getString("status").toString())
//                if (succses == 200) {
//                    var data = JSONArray()
//                    data = jsonObject.getJSONArray("data")
//
//                    if (data.length() >= 0) {
//                        for (i in 0 until data.length()) {
//                            val mprod = ModelProduct()
//                            mprod.setHeader(code)
//                            mprod.setIdProd(data.getJSONObject(i).getString("id_product"))
//                            mprod.setItem(data.getJSONObject(i).getString("nama"))
//                            mprod.setDesc(data.getJSONObject(i).getString("description"))
//                            mprod.setPrice(data.getJSONObject(i).getString("harga"))
//                            mprod.setDisc(data.getJSONObject(i).getString("discount"))
//                            mprod.setCrystal(data.getJSONObject(i).getString("crystal"))
//                            mprod.setType(data.getJSONObject(i).getString("type"))
//                            mprod.setDetail(data.getJSONObject(i).getString("detail"))
//
//                            Api.prodList!!.add(mprod)
//                        }
//                        return true
//
//                    } else {
//
//                        return false
//
//                    }
//
//                } else {
//                    return false
//                }
//
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//
//            return false
//        }
//
//        fun handleHome(sjson: String, context: Context, view: View): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//
//                    val json = JSONObject(jsonObject.getJSONObject("data").toString() + "")
//                    Helper.setText(R.id.txt_tgl, view, json.getString("tanggal"))
//                    Helper.setText(
//                        R.id.txt_usrtotal,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json.getString("user_all")))
//                    )
//                    Helper.setText(
//                        R.id.txt_usronline,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json.getString("user_online")))
//                    )
//                    Helper.setText(R.id.txt_usronlinepercent, view, json.getString("user_online_persentase") + "%")
//                    Helper.setText(
//                        R.id.txt_usroffline,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json.getString("user_ofline")))
//                    )
//                    Helper.setText(
//                        R.id.txt_omseth1,
//                        view,
//                        Helper.getNumberFormatCurrency(Integer.parseInt(json.getString("omset_bulan_lalu")))
//                    )
//                    Helper.setText(
//                        R.id.txt_cashout,
//                        view,
//                        Helper.getNumberFormatCurrency(Integer.parseInt(json.getString("pengeluaran_bulan_lalu")))
//                    )
//                    Helper.setText(
//                        R.id.txt_netto,
//                        view,
//                        Helper.getNumberFormatCurrency(
//                            Integer.parseInt(json.getString("omset_bulan_lalu")) - Integer.parseInt(json.getString("pengeluaran_bulan_lalu"))
//                        )
//                    )
//                    Helper.setText(
//                        R.id.txt_registration,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json.getString("user_registrasi_week")))
//                    )
//                    Helper.setText(
//                        R.id.txt_active,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json.getString("user_active_week")))
//                    )
//
//                    return true
//
//                } else {
//
//                    return false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
//
//        fun handleDoUn(sjson: String, context: Context, view: View): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//                    val json_data = JSONObject(jsonObject.getJSONObject("data").toString() + "")
//                    val json_day = JSONObject(json_data.getJSONObject("day").toString() + "")
//                    val json_month = JSONObject(json_data.getJSONObject("month").toString() + "")
//                    val json_year = JSONObject(json_data.getJSONObject("year").toString() + "")
//
//                    //install
//                    Helper.setText(
//                        R.id.txt_todown,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json_day.getString("install")))
//                    )
//                    Helper.setText(
//                        R.id.txt_modown,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json_month.getString("install")))
//                    )
//                    Helper.setText(
//                        R.id.txt_yedown,
//                        view,
//                        Helper.getNumberFormat(Integer.parseInt(json_year.getString("install")))
//                    )
//
//                    return true
//
//                } else {
//
//                    return false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
//
//        fun handleDataTamu(sjson: String, context: Context, view: View): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
//                    val json_data = jsonObject.getJSONObject("data")
//                    val result = json_data.getJSONArray("result")
//
//                    if (result.length() > 0) {
//                        for (i in 0 until result.length()) {
//                            val tamu = ValueFilter()
//                            tamu.id = (result.getJSONObject(i).getString("id"))
//                            tamu.name = (result.getJSONObject(i).getString("name"))
//                            tamu.email = (result.getJSONObject(i).getString("email"))
//                            tamu.phone = (result.getJSONObject(i).getString("phone"))
//                            tamu.alamat = (result.getJSONObject(i).getString("alamat"))
//                            tamu.komentar = (result.getJSONObject(i).getString("komentar"))
//                            Api.dataList.add(tamu)
//                        }
//
//                        return true
//
//                    } else {
//
//                        return false
//                    }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//            return false
//        }

        fun handleRegister(sjson: String, context: Context): Boolean {

            val session = Session(context)
            try {
                val jsonObject = JSONObject(sjson)

                return true

            } catch (e: JSONException) {

            } catch (e: Exception) {

            }

            return false
        }
//
//
//        fun handleTransaksiDetail(sjson: String, context: Context): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//                    val data = jsonObject.getJSONArray("data")
//                    if (data.length() > 0) {
//                        for (i in 0 until data.length()) {
//                            val transaksi = ModelTransaksi()
//                            transaksi.setNama(data.getJSONObject(i).getString("nama"))
//                            transaksi.setQty(data.getJSONObject(i).getString("qty"))
//                            transaksi.setHarga(data.getJSONObject(i).getString("harga"))
//                            transaksi.setTotal(data.getJSONObject(i).getString("total"))
//                            Api.transList!!.add(transaksi)
//
//                        }
//                        return true
//
//                    } else {
//
//                        return false
//                    }
//
//                } else {
//
//                    return false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
//
//        fun handleUserActive(sjson: String, context: Context): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//                    val data = jsonObject.getJSONArray("data")
//                    if (data.length() > 0) {
//                        for (i in 0 until data.length()) {
//                            val user = ModelUser()
//                            user.setEmail(data.getJSONObject(i).getString("email"))
//                            user.setRegistrasi(data.getJSONObject(i).getString("registrasi"))
//                            user.setLast_login(data.getJSONObject(i).getString("last_login"))
//
//                            Api.userList!!.add(user)
//
//                        }
//                        return true
//
//                    } else {
//
//                        return false
//                    }
//
//                } else {
//
//                    return false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
//
//        fun handleAnggaran(sjson: String, context: Context): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//                if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//                    val data = jsonObject.getJSONArray("data")
//                    if (data.length() > 0) {
//                        for (i in 0 until data.length()) {
//                            val manggaran = ModelAnggaran()
//                            manggaran.setIdrab(data.getJSONObject(i).getString("id_rab"))
//                            manggaran.setRabname(data.getJSONObject(i).getString("rab_name"))
//                            manggaran.setCreatedat(data.getJSONObject(i).getString("created_at"))
//                            manggaran.setNominal(data.getJSONObject(i).getString("nominal"))
//
//                            Api.anggaranList!!.add(manggaran)
//
//                        }
//                        return true
//
//                    } else {
//
//                        return false
//
//                    }
//
//                } else {
//                    return false
//                }
//
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//
//            return false
//        }
//
//        fun handleApproveRAB(sjson: String, context: Context): String {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                return if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//
//                    jsonObject.getString(context.getString(R.string.status))
//
//                } else {
//
//                    jsonObject.getString(context.getString(R.string.status))
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return "Gagal, Silahkan coba lagi"
//        }
//
//        fun handleApprovePlay(sjson: String, context: Context): Boolean {
//
//            try {
//                val jsonObject = JSONObject(sjson)
//
//                return if (jsonObject.getString(context.getString(R.string.status)) == "200") {
//
//
//                    true
//
//                } else {
//
//                    false
//
//                }
//
//            } catch (e: JSONException) {
//
//            } catch (e: Exception) {
//
//            }
//
//            return false
//        }
    }


}
