package com.dev.merapitech.guestbook.model.value

class ValueRegister {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: DataRegister
    class DataRegister{
        lateinit var name: String
        lateinit var email: String
        lateinit var phone: String
        lateinit var alamat: String
        lateinit var komentar: String
        lateinit var updated_at: String
        lateinit var created_at: String
        lateinit var id: String
    }
}