package com.dev.merapitech.guestbook

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.dev.merapitech.guestbook.adapter.FilterAdapter
import com.dev.merapitech.guestbook.model.request.RequestRegister
import com.dev.merapitech.guestbook.model.value.ValueRegister
import com.dev.merapitech.guestbook.utils.Api
import com.dev.merapitech.guestbook.utils.Handle
import com.dev.merapitech.guestbook.utils.ParamReq
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_add_guest.*
import kotlinx.android.synthetic.main.activity_add_guest.button_check_in
import kotlinx.android.synthetic.main.activity_add_guest.toolbar
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddGuestActivity : AppCompatActivity() {

    lateinit var mApiService: BaseApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_guest)

        mApiService = UtilsApi.apiService

        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this@AddGuestActivity, MainActivity::class.java))
        }

        button_check_in.setOnClickListener {
            if (!TextUtils.isEmpty(et_name.text) && !TextUtils.isEmpty(et_mail.text) && !TextUtils.isEmpty(et_phone.text) && !TextUtils.isEmpty(et_address.text)){
                val req = RequestRegister()
                req.HeaderCode = "0104"
                req.signature = getString(R.string.signature)
                val data = RequestRegister.RegisterData()
                data.name = et_name.text.toString()
                data.email = et_mail.text.toString()
                data.phone = et_phone.text.toString()
                data.alamat = et_address.text.toString()
                req.data = data
                mApiService.registerGuest(req)
                    .enqueue(object : Callback<ValueRegister>{
                        override fun onResponse(call: Call<ValueRegister>, response: Response<ValueRegister>) {
                            if (response.isSuccessful){
                                if (response.body()!!.status == "200"){
                                    Toast.makeText(applicationContext, "Check In Success !!", Toast.LENGTH_SHORT).show()
                                    startActivity(Intent(this@AddGuestActivity, MainActivity::class.java))
                                    this@AddGuestActivity.finish()
                                } else {
                                    val message = response.body()!!.message
                                    Toast.makeText(this@AddGuestActivity, message, Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                Log.d("pg", "asdasd")
                                Toast.makeText(applicationContext, "Input Data Error !!", Toast.LENGTH_SHORT).show()
                            }
                        }
                        override fun onFailure(call: Call<ValueRegister>, t: Throwable) {
                            AlertDialog.Builder(this@AddGuestActivity)
                                .setTitle("Email tidak sesuai")
                                .setMessage("Pastikan email diisi sesuai standard penulisan email")
                                .setCancelable(false)
                                .setNegativeButton("Coba Lagi", DialogInterface.OnClickListener { arg0, arg1 ->

                                }
                                ).create().show()
                        }
                    })
            }else {
                Toast.makeText(applicationContext, "Semua kolom harap diisi!!!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
